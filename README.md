广州技客 Thinkphp6 工具类
=======================
### 该工具来源于网络，因为项目使用php8，之前的包有问题，故自己修改了下复制了个供高版本的有需要客户使用

### 安装
~~~
composer require ric/thinkphp6-avatars
~~~


```php
<?php
require(dirname(__FILE__) . "/src/MDAvatars.php");

/*
'X'         : 需要生成头像的字符串
512 (pixel) : 头像大小
*/

$Avatar = new \ric\avatars\MDAvatars('X', 512);
?>
```

Or initialize with composer: 


```json
{
	"require": {
		"lincanbin/material-design-avatars": "*.*"
	}
}
```
```php
<?php
// composer 
require(__DIR__ . "/vendor/autoload.php");
use \ric\avatars\MDAvatars;

$Avatar = new MDAvatars('X', 512);
?>
```

Usage
------------

#### Show you avatar in the browser

```php
<?php
$Avatar->Output2Browser();
?>
```

```php
<?php
// You can resize the ouput size again here.
$OutputSize = 256;
$Avatar->Output2Browser($OutputSize);
// Output Base64 encoded image data.
$Avatar->Output2Base64($OutputSize);
// Get an image resource identifier.
$Avatar->Output2ImageResource($OutputSize);
?>
```

#### Save avatar to a file

```php
<?php
$Avatar->Save('./avatars/Avatar.png');
//You can resize the size you want to save again here.
$Avatar->Save('./avatars/Avatar256.png', 256);
$Avatar->Save('./avatars/Avatar128.png', 128);
$Avatar->Save('./avatars/Avatar64.png', 64);
?>
```

#### Free memory

```php
<?php
$Avatar->Free();
?>
```

